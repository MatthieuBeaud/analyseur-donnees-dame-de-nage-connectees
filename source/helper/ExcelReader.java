package helper;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

//import org.apache.poi.hssf.usermodel.HSSFWorkbook;	//if it's a xls file
//import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFSheet;			//if it's a xlsx file
import org.apache.poi.xssf.usermodel.XSSFWorkbook;


public class ExcelReader {
	
	public static XSSFWorkbook openExcel(File file) throws IOException {
		FileInputStream fis = new FileInputStream(file);
		return new XSSFWorkbook(fis);
	}
	
	public static XSSFWorkbook openExcel(FileInputStream fIS) throws IOException {
		return new XSSFWorkbook(fIS);
	}
	
	public static XSSFSheet getSheet(XSSFWorkbook wb, int numSheet) throws Exception{
		return wb.getSheetAt(numSheet);
	}
	
	public static Double getCellValue(XSSFSheet sheet, int col, int row) throws Exception{
		return sheet.getRow(row).getCell(col).getNumericCellValue();
	}
	
	public static String getCellContent(XSSFSheet sheet, int col, int row) throws Exception{
		return sheet.getRow(row).getCell(col).getStringCellValue();
	}
	
	public static Timestamp getCellTime(XSSFSheet sheet, int col, int row) throws Exception{
		return new Timestamp(sheet.getRow(row).getCell(col).getDateCellValue().getTime());
	}
	
	public static List<Double> getColValues(XSSFSheet sheet, int colNum, int startIndex){
		ArrayList<Double> values = new ArrayList<Double>();
		int lastIndex = sheet.getLastRowNum();
		for(int i=startIndex; i<=lastIndex; i++) {
			try {
				values.add(sheet.getRow(i).getCell(colNum).getNumericCellValue());
			}catch (Exception ex) {
				System.out.println(ex);
			}
		}
		return values;
	}
	
	public static List<Double> getColValuesFromStrings(XSSFSheet sheet, int colNum, int startIndex){
		ArrayList<Double> values = new ArrayList<Double>();
		int lastIndex = sheet.getLastRowNum();
		for(int i=startIndex; i<=lastIndex; i++) {
			try {
				values.add(Double.parseDouble(sheet.getRow(i).getCell(colNum).getStringCellValue()));
			}catch (Exception ex) {
				System.out.println(ex);
			}
		}
		return values;
	}
	
	public static List<Double> getColValues(XSSFSheet sheet, int colNum, int startIndex, int endIndex){
		ArrayList<Double> values = new ArrayList<Double>();
		for(int i=startIndex; i<=endIndex; i++) {
			try {
				values.add(sheet.getRow(i).getCell(colNum).getNumericCellValue());
			}catch (Exception ex) {
				System.out.println(ex);
			}
		}
		return values;
	}
	
	public static int findLine(XSSFSheet sheet,int col1, String val1, int col2, String val2) {
		int numLigne = -1;
		String content1 = "";
		String content2 = "";
		for(int i=0; i<sheet.getLastRowNum(); i++) {
			try {
				content1 = getCellContent(sheet, col1, i);
				content2 = getCellContent(sheet, col2, i);
			}catch(Exception e) {
				
			}
			if(val1.equals(content1) && val2.contentEquals(content2)) {
				numLigne = i;
				break;
			}
			
		}
		return numLigne;
	}

}
