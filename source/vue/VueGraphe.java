package vue;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.List;

import javax.swing.JPanel;

public class VueGraphe extends JPanel{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	List<String> axisNames; 
	List<Double> maxValues;
	List<Double> values;
	
	int h;
	int w;
	
	int midX;
	int midY;
	
	int nbOfAxis;

	public VueGraphe(List<String> names, List<Double> vals, List<Double> refs) {
		super();
		this.setBackground(Color.white);
		axisNames=names;
		maxValues=refs;
		values=vals;
		
		h=this.getHeight();
		w=this.getWidth();
		midX=w/2;
		midY=h/2;
		
		nbOfAxis = Math.min(axisNames.size(), Math.min(maxValues.size(), values.size()));
		while(axisNames.size()>nbOfAxis) {
			axisNames.remove(axisNames.size());
		}
		while(maxValues.size()>nbOfAxis) {
			maxValues.remove(maxValues.size());
		}
		while(values.size()>nbOfAxis) {
			values.remove(values.size());
		}
	}
	
	private int[] getXPointsOut() {
		int[] xPts = new int[nbOfAxis];
		for(int i=0; i<nbOfAxis; i++) {
			xPts[i]= (int)(midX + 0.8*w/2*Math.cos(i*2*Math.PI/nbOfAxis)); 
		}
		return xPts;
	}
	
	private int[] getYPointsOut() {
		int[] yPts = new int[nbOfAxis];
		for(int i=0; i<nbOfAxis; i++) {
			yPts[i]= (int)(midY + 0.8*h/2*Math.sin(i*2*Math.PI/nbOfAxis)); 
		}
		return yPts;
	}
	
	private int[] getXPointsIn() {
		int[] xPts = new int[nbOfAxis];
		for(int i=0; i<nbOfAxis; i++) {
			xPts[i]= (int)(midX + values.get(i)/maxValues.get(i)*0.8*w/2*Math.cos(i*2*Math.PI/nbOfAxis)); 
		}
		return xPts;
	}
	
	private int[] getYPointsIn() {
		int[] yPts = new int[nbOfAxis];
		for(int i=0; i<nbOfAxis; i++) {
			yPts[i]= (int)(midY + values.get(i)/maxValues.get(i)*0.8*h/2*Math.sin(i*2*Math.PI/nbOfAxis)); 
		}
		return yPts;
	}
	
	private void drawAxisName(Graphics g) {
		int[] xPts = getXPointsOut();
		int[] yPts = getYPointsOut();
		for(int i=0; i<nbOfAxis; i++) {
			g.drawChars(axisNames.get(i).toCharArray(), 0, axisNames.get(i).length(), xPts[i], yPts[i]);
		}
	}
	
	private void drawAxis(Graphics g) {
		g.setColor(Color.GRAY);
		for(int i=0; i<nbOfAxis; i++) {
			g.drawLine(midX, midY, (int)(midX + 0.9*w/2*Math.cos(i*2*Math.PI/nbOfAxis)), (int)(midY + 0.9*h/2*Math.sin(i*2*Math.PI/nbOfAxis)));
		}
	}
	
	private void drawOuterPoly(Graphics g) {
		g.setColor(Color.LIGHT_GRAY);
		g.fillPolygon(getXPointsOut(), getYPointsOut(), nbOfAxis);
		g.setColor(Color.DARK_GRAY);
		g.drawPolygon(getXPointsOut(), getYPointsOut(), nbOfAxis);
	}
	
	private void drawInnerPoly(Graphics g) {
		g.setColor(Color.DARK_GRAY);
		g.drawPolygon(getXPointsIn(), getYPointsIn(), nbOfAxis);
	}
	
	
	@Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        
        h=this.getHeight();
		w=this.getWidth();
		midX=w/2;
		midY=h/2;
		drawOuterPoly(g);
		drawAxis(g);
		drawInnerPoly(g);
		drawAxisName(g);
	}
}
