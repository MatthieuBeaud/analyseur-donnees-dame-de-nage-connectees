package vue;
import javax.swing.*;
import java.awt.event.*;



public class FenetreAide extends JFrame implements ActionListener{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel conteneur;
	private JButton suiteD;
	private JButton suiteG;
	private JLabel t1;
	private JLabel t2;
	private JLabel t3;	
	private JLabel[] tabTexts;
	int page;	

	public FenetreAide(){
		super("Aide");
		this.setLayout(null);
        this.setSize(600,300);
        this.setLocation(600,75);
        // Pour empêcher le redimensionnement de la fenêtre
		this.setResizable(false);

		tabTexts= new JLabel[3];
		
		suiteD= new JButton(">");
		suiteD.setBounds(540,110,50,30);
		
		suiteG= new JButton("<");
		suiteG.setBounds(10,110,50,30);
		
		t1= new JLabel("<html> Pour commencer, remplissez les informations <br/>"
				+ " concernant le rameur : <br/>"
				+ " - nom <br/> "
				+ "- prenom <br/> "
				+ "- taille (en cm) (ex: 180) <br/> "
				+ "- poids (en kg) (ex: 72.5) <br/>"
				+ " - categorie (selectionner parmis celles proposees) <br/>  <br/>  </html>",SwingConstants.CENTER);
		t1.setBounds(65, 0,470,260);
		
		t2= new JLabel("<html> Puis, selectionnez le fichier grace a  <br/> "
				+ "l'explorateur de fichier present lors  <br/> "
				+ "de l'appui sur le bouton 'choisir fichier' <br/>  <br/> </html>",SwingConstants.CENTER);
		t2.setBounds(65,0,470,260);
		
		t3= new JLabel("<html> Le fichier choisi doit etre au format Excel .xlsx <br/> "
				+ "A partir d'un fichier de donnees brutes (.csv) faire les operations suivantes: <br/> "
				+ "- l'ouvrir avec excel <br/> "
				+ "- selectionner la colonne (ou la case) ou sont ecrites les donnees<br/> "
				+ "- aller dans l'onglet donnees <br/> - cliquer sur convertir <br/> "
				+ "- selectionner  ,  (virgule) comme caractere de separation <br/> "
				+ "- cliquer sur fin <br/> "
				+ "- enregistrer le fichier sous le format .xlsx</html>",SwingConstants.CENTER);
		t3.setBounds(65,0,470,260);
		
		conteneur= new JPanel();
        conteneur.setLayout(null);
        conteneur.add(suiteG);
        conteneur.add(suiteD);
        conteneur.add(t1);
        conteneur.add(t2);
        conteneur.add(t3);
        suiteD.addActionListener(this);
        suiteG.addActionListener(this);
        
        page= 1;
		tabTexts[0] = t1;
		tabTexts[1] = t2;
		tabTexts[2] = t3;
		afficherPage(page);
        
		this.setContentPane(conteneur);
		this.setVisible(true);
	}
	
	
	
	public void actionPerformed(ActionEvent e){
		if(e.getSource()==suiteD && page<3){
			page++;
			afficherPage(page);
		}
		if(e.getSource()==suiteG && page>1){
			page--;
			afficherPage(page);
		}
	}
	
	public void afficherPage(int numP){
		for(int i=0; i<tabTexts.length; i++){
			if(i==numP-1){
				tabTexts[i].setVisible(true);
			}else{
				tabTexts[i].setVisible(false);
			}
		}
	}
}
