package vue;

import model.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.ArrayList;

import javax.swing.filechooser.*;

public class FenetreInfo extends JFrame implements ActionListener {
    
	/**
	 * 
	 */
	private static final long serialVersionUID = 4628591652391045704L;
	final String VERSION_NUMBER = "4.4";
	
	final String[] boat = {"1x", "2-", "2x", "4-", "4+", "4x", "8+"};
	final String[] boatPLH = {"1x", "2-", "2x", "4-", "4x", "8+"};
	final String[] boatPLF = {"1x", "2x", "4x"};
	
	private JPanel conteneur;
	
    private JTextField textnom;
    private JTextField textprenom;
    private JTextField texttaille;
    private JTextField textpoids;
    private JLabel afficheAdresseDonnees;   
    private JLabel afficheAdresseRef;
    private JButton lancer;
    private JButton enregistrer;
    private JButton choisirFichierDonnees;
    private JButton choisirFichierRef;
    private JButton aide;
    private JComboBox<String> choixCategorie;
    private JComboBox<String> choixBoatClass;
    private JComboBox<String> choixEntrainement;
    private Athlete athlete;
    private Donnees donnees;
    private DonneesReference donneesRef;
    @SuppressWarnings("unused")
	private FenetreResultat fResults;
    @SuppressWarnings("unused")
	private FenetreAnalyse fAnalyse;
    @SuppressWarnings("unused")
	private FenetreAide fAide;
    final JFileChooser fc;
    private String pathDonnees;
    private String pathDonneesRef;
    boolean erreur;
    

    
    public FenetreInfo(){
        super("Fenetre d'informations");
        this.setLayout(null);
        this.setSize(510,550);
        this.setLocation(75,75);
        // Pour empêcher le redimensionnement de la fenêtre
		this.setResizable(false);
		// Pour permettre la fermeture de la fenêtre lors de l'appui sur la croix rouge
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
		
		//preparation du syteme de recuperation de fichier
			
		fc = new JFileChooser("D:\\Matthieu\\Travail\\Informatique\\Programmes\\Progamme Analyse Biometrique\\Données Excel");       //ouverture de l'explorateur de documents pour ouvrir le fichier excel
		javax.swing.filechooser.FileFilter filter = new FileNameExtensionFilter("fichier Excel compatible", "xlsx");
		fc.addChoosableFileFilter(filter);
		fc.setFileFilter(filter);        // ajout d'un filtre qui permet de ne voir que les fichiers xls (excel 2003)
				
        //creation des textes, boutons etc
		
		
		
        JLabel nom = new JLabel();
        nom.setBounds(20,50,150,50);
        nom.setText("Nom: ");
        textnom = new JTextField();
        textnom.setBounds(140,50, 150, 50);
        
        JLabel prenom = new JLabel();
        prenom.setBounds(20,100,150,50);
        prenom.setText("Prenom: ");
        textprenom = new JTextField();
        textprenom.setBounds(140,100, 150, 50); 
        
        JLabel taille = new JLabel();
        taille.setBounds(20,150,150,50);
        taille.setText("Taille: ");
        texttaille = new JTextField();
        texttaille.setBounds(140,150, 150, 50); 
        
        JLabel poids = new JLabel();
        poids.setBounds(20,200,150,50);
        poids.setText("Poids: ");
        textpoids = new JTextField();
        textpoids.setBounds(140,200, 150, 50); 
        
        
        choisirFichierDonnees = new JButton("Choisir fichier Données");
        choisirFichierDonnees.setBounds(50,275,180,50);
        
        choisirFichierRef = new JButton("Choisir fichier Ref");
        choisirFichierRef.setBounds(275,275,180,50);
        
        afficheAdresseDonnees = new JLabel();
        afficheAdresseDonnees.setBounds(50,320,240,50);
        afficheAdresseDonnees.setText("");
        
        afficheAdresseRef = new JLabel();
        afficheAdresseRef.setBounds(275,320,240,50);
        afficheAdresseRef.setText("");
        
        JLabel auteur=new JLabel();
        auteur.setBounds(5,0,300,20);
        Font font=new Font("Comic Sans MS",3,10);
        auteur.setFont(font);
        auteur.setForeground(Color.gray);
        auteur.setText("Version "+ VERSION_NUMBER +"  Auteur: Matthieu BEAUD");
        
        
        lancer= new JButton("Nouvel Entrainement");
        lancer.setBounds(150,380, 200,60);
        
        enregistrer = new JButton("Enregistrer");
        enregistrer.setBounds(150, 450, 200,60);
        
        String[] choixCat = { "Senior Homme", "Seniore Femme", "Senior Homme PL", "Seniore Femme PL", "Junior Homme", "Juniore Femme"};
        
        choixCategorie= new JComboBox<String>(choixCat);
        choixCategorie.setBounds(350,50,140,40);
        
        String[] choixBoat = {"1x", "2-", "2x", "4-", "4+", "4x", "8+"};
        
        choixBoatClass= new JComboBox<String>(choixBoat);
        choixBoatClass.setBounds(350,100,70,40);
        
        String[] intensitesEntrainement = {"B1", "B2", "B3", "B4", "B5", "Course"};
        
        choixEntrainement= new JComboBox<String>(intensitesEntrainement);
        choixEntrainement.setBounds(350,150,70,40);
        
        aide= new JButton("Aide");
        aide.setBounds(380,460,100,40);
        //ajout de tout au conteneur
        
        conteneur= new JPanel();
        conteneur.setLayout(null);
        conteneur.add(prenom);
        conteneur.add(nom);
        conteneur.add(textnom);
        conteneur.add(textprenom);
        conteneur.add(taille);
        conteneur.add(texttaille);
        conteneur.add(poids);
        conteneur.add(textpoids);
        conteneur.add(afficheAdresseDonnees);
        conteneur.add(choisirFichierDonnees);
        conteneur.add(afficheAdresseRef);
        conteneur.add(choisirFichierRef);
        conteneur.add(auteur);
        conteneur.add(lancer);
        conteneur.add(enregistrer);
        conteneur.add(aide);
        enregistrer.setVisible(false);
        choisirFichierDonnees.addActionListener(this);
        choisirFichierRef.addActionListener(this);
        conteneur.add(choixCategorie);
        choixCategorie.addActionListener(this);
        conteneur.add(choixBoatClass);
        conteneur.add(choixEntrainement);
        lancer.addActionListener(this);
        enregistrer.addActionListener(this);
        aide.addActionListener(this);
        this.setContentPane(conteneur);
        this.setVisible(true);
    }
    
    private File selectionneFichier() {
    	int returnVal = fc.showDialog(FenetreInfo.this, "Ouvrir");   //recuperation du chemin du fichier choisi
		if (returnVal == JFileChooser.APPROVE_OPTION) {
            File file = fc.getSelectedFile();
		    return file;
		}
		return null;
    }
    
    private void selectionnerFichierDonnees() {
	    try{
	    	File fichierDonnees = selectionneFichier();
	    	pathDonnees = fichierDonnees.getAbsolutePath();
			afficheAdresseDonnees.setText(fichierDonnees.getName());
		}catch (Exception a){
			JOptionPane.showMessageDialog(null,"erreur, le fichier choisi n'est au format souhaité (.xlsx)");    //gestion de l'erreur
		}
    }
    
    private void selectionnerFichierRef() {
    	try{
    		File fichierDonneesRef = selectionneFichier();
	    	pathDonneesRef = fichierDonneesRef.getAbsolutePath();
			afficheAdresseRef.setText(fichierDonneesRef.getName());
		}catch (Exception a){
			JOptionPane.showMessageDialog(null,"erreur, le fichier choisi n'est au format souhaité (.xlsx)");    //gestion de l'erreur
		}
    }

	
	public void saveFile() throws IOException{
		String saveName = (athlete.getNom()+"_"+athlete.getPrenom()+"_"+donnees.laDate()+".txt");
		String dir=System.getProperty("user.dir");
		File file= new File(dir+"\\"+saveName);
		PrintWriter writer = new PrintWriter(file,"UTF-8");
		writer.println("Date: "+donnees.laDate());
		writer.println();
		writer.println("Identite:");
		writer.println("	Nom: "+athlete.getNom() );
		writer.println("	Prenom: "+athlete.getPrenom());
		writer.println("	Taille: "+athlete.getTaille());
		writer.println("	Poids: "+athlete.getPoids());
		writer.println("	Categorie: "+athlete.categorie);
		writer.println();
		writer.println("Parametres: ");
		writer.println("	Longueur pelle: "+donnees.paramPelle[0]+" cm");
		writer.println(" 	Levier: "+donnees.paramPelle[1]+" cm");
		writer.println();
		for(int i=1; i<23; i++){
			writer.println(donnees.textes.get(i));
			if(i==2 || i==6 || i==10 || i== 19 || i==20){ //pour aerer le texte enregistrer (differencier les differentes categories d'info)
				writer.println();
			}
		}
		writer.close();
		JOptionPane.showMessageDialog(this,"Enregistrement reussi !");
	}
		
	
	public void actionPerformed(ActionEvent e){
		if(e.getSource()==choisirFichierDonnees){
			selectionnerFichierDonnees();
		}
		
		if(e.getSource()==choisirFichierRef){
			selectionnerFichierRef();
		}
		
        if(e.getSource()==lancer){
            //Peut etre rajouter les verification que les entrées sont bien des int, utiliser fenetre erreur pop up
            try{
				float p = Float.parseFloat(textpoids.getText());
				int t = Integer.parseInt(texttaille.getText());
				String n = textnom.getText();
				String pn = textprenom.getText();
				String categorie = (String)choixCategorie.getSelectedItem();
				String boatClass = (String)choixBoatClass.getSelectedItem();
				athlete= new Athlete(t,p,n,pn, categorie, boatClass);
            }catch(Exception exc) {
            	JOptionPane.showMessageDialog(this,"Il faut que la taille soit un nombre entier, et pour le poids, mettre un . au lieu d'une , ");
            }
            try{
				donnees = new Donnees(pathDonnees, athlete, choixBoatClass.getSelectedItem().toString());
			}catch (Exception ex){
				JOptionPane.showMessageDialog(this,"Erreur lors de la récupération des données");
				JOptionPane.showMessageDialog(this,ex.toString());
				erreur=true;
			}try {
				donneesRef = new DonneesReference(pathDonneesRef, athlete, (String)choixEntrainement.getSelectedItem());
			}catch (Exception ex){
				JOptionPane.showMessageDialog(this,"Erreur lors de la récupération des données de référence");
				JOptionPane.showMessageDialog(this,ex.toString());
				erreur=true;
			}
            if(!erreur){
				try{
		            fResults= new FenetreResultat(athlete, donnees);
					enregistrer.setVisible(true);
					fAnalyse = new FenetreAnalyse(donnees, donneesRef, athlete);
					
				}catch (Exception a){
					JOptionPane.showMessageDialog(this,"Erreur lors du traitement des données");
					a.printStackTrace();
				}
			}
        }
        if(e.getSource()==enregistrer){
			try{
				saveFile();
			}catch (IOException c){
				JOptionPane.showMessageDialog(this,"Erreur d'enregistrement");
				c.printStackTrace();
			}
		}
		if(e.getSource()==aide){
			fAide=new FenetreAide();
		}
		if(e.getSource()==choixCategorie) {
			if(choixCategorie.getSelectedItem().equals("Senior Homme PL")) {
				choixBoatClass.setModel(new DefaultComboBoxModel<String>(boatPLH));
			}else if(choixCategorie.getSelectedItem().equals("Seniore Femme PL")) {
				choixBoatClass.setModel(new DefaultComboBoxModel<String>(boatPLF));
			}else {
				choixBoatClass.setModel(new DefaultComboBoxModel<String>(boat));
			}
		}
    }
}
