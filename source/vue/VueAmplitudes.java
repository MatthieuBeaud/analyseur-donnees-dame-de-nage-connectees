package vue;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.List;

import javax.swing.JPanel;

public class VueAmplitudes extends JPanel{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8109565103871307881L;
	double angleA;
	double angleFM;
	double angleS;
	double slipA;
	double slipS;
	
	int h;
	int w;
	
	int midX;
	int midY;
	
	int ddnTribordY;
	int ddnTribordX;
	int ddnBabordY;
	int ddnBabordX;
	int boatWidth;
	int offset;
	int bowBallDiameter;
	

	
	/**
	 * Constructor
	 * @param 
	 * angles la liste des angles en degré dans l'ordre suivant:
	 * angle attaque
	 * angle force max
	 * angle sortie
	 * slip attaque
	 * slip sortie
	 */
	public VueAmplitudes(List<Double> angles) {
		super();
		this.setBackground(Color.lightGray);
		angleA = angles.get(0)/180*Math.PI;
		angleFM = angles.get(1)/180*Math.PI;
		angleS = angles.get(2)/180*Math.PI;
		slipA = angles.get(3)/180*Math.PI;
		slipS = angles.get(4)/180*Math.PI;
		
		updateDims();
	}
	
	/**
	 * Constructor
	 * @param angles la liste des angles en degré dans l'ordre suivant:
	 * angle attaque
	 * angle force max
	 * angle sortie
	 * slip attaque
	 * slip sortie
	 */
	public VueAmplitudes(double angleAttaque, double angleForceMax, double angleSortie, double slipAvant, double slipSortie) {
		super();
		this.setBackground(Color.lightGray);
		angleA = angleAttaque/180*Math.PI;
		angleFM = angleForceMax/180*Math.PI;
		angleS = angleSortie/180*Math.PI;
		slipA = slipAvant/180*Math.PI;
		slipS = slipSortie/180*Math.PI;
		
		updateDims();
	}
	
	private void updateDims() {
		h=this.getHeight();
		w=this.getWidth();
		midX=w/2;
		midY=h/2;
		
		boatWidth = h/20;
		offset = h/10;
		bowBallDiameter = 10;
		
		ddnTribordY=midY;
		ddnTribordX=midX+3*boatWidth;
		ddnBabordY=midY;
		ddnBabordX=midX-3*boatWidth;
	}
	
	private void drawBoat(Graphics g) {
		updateDims();
		Graphics2D g2 = (Graphics2D)g;
		
		int[] xPoints= {midX, midX-boatWidth, midX-boatWidth, midX, midX+boatWidth, midX+boatWidth, midX};
		int[] yPoints= {bowBallDiameter,midY-offset, midY+offset, h-bowBallDiameter, midY+offset, midY-offset, bowBallDiameter};
		int nPoints = 7;
		g.drawPolygon(xPoints, yPoints, nPoints);
		
		//bowBall
		g.fillOval(midX-bowBallDiameter/2, bowBallDiameter/2, bowBallDiameter, bowBallDiameter);
		
		//slides
		g.setColor(Color.DARK_GRAY);
		g2.setStroke(new BasicStroke(5));
		g.drawLine(midX-boatWidth/2, midY-offset, midX-boatWidth/2, midY+offset/2);
		g.drawLine(midX+boatWidth/2, midY-offset, midX+boatWidth/2, midY+offset/2);
		g2.setStroke(new BasicStroke(1));
		g.setColor(Color.black);
				
		//wingRigger
		int[] xPointsR = {ddnBabordX, midX-boatWidth, midX+boatWidth, ddnTribordX};
		int[] yPointsR = {ddnBabordY, midY-offset, midY-offset, ddnTribordY};
		int nPointsR = 4;
		
		g2.setStroke(new BasicStroke(15));
		g2.drawPolyline(xPointsR, yPointsR, nPointsR);
		g2.setStroke(new BasicStroke(1));
		
		//shoes
		int sizeShoes= boatWidth/3;
		g.fillOval(midX-boatWidth/3-sizeShoes/2, midY+offset-3*sizeShoes/2, sizeShoes, 2*sizeShoes);
		g.fillOval(midX+boatWidth/3-sizeShoes/2, midY+offset-3*sizeShoes/2, sizeShoes, 2*sizeShoes);
		g.drawLine(midX-boatWidth, midY+offset, midX+boatWidth, midY+offset);
		
		//seat
		int holeSize= boatWidth/4;
		g.fillRoundRect(midX-2*boatWidth/3, midY-2*boatWidth/3, 4*boatWidth/3, 2*boatWidth/3, holeSize, holeSize);
		g.setColor(this.getBackground());
		g.fillOval(midX-3*holeSize/2, midY-boatWidth/3-holeSize/2, holeSize, holeSize);
		g.fillOval(midX+holeSize/2, midY-boatWidth/3-holeSize/2, holeSize, holeSize);
	}
	
	private void drawAmplitudes(Graphics g) {
		int longPelle = 10000;
		Graphics2D g2 = (Graphics2D)g;
		g2.setStroke(new BasicStroke(2));
		//babord
		g.setColor(Color.red);
		g.drawLine(ddnBabordX, ddnBabordY, (int)(ddnBabordX-longPelle*Math.cos(angleA-slipA)), (int)(ddnBabordY+longPelle*Math.sin(angleA-slipA)));
		drawAngleText(g, angleA-slipA);
		g.drawLine(ddnBabordX, ddnBabordY, (int)(ddnBabordX-longPelle*Math.cos(angleS+slipS)), (int)(ddnBabordY+longPelle*Math.sin(angleS+slipS)));
		drawAngleText(g, angleS+slipS);
		g.setColor(new Color(15,255,15));	//green
		g.drawLine(ddnBabordX, ddnBabordY, (int)(ddnBabordX-longPelle*Math.cos(angleA)), (int)(ddnBabordY+longPelle*Math.sin(angleA)));
		drawAngleText(g, angleA);
		g.drawLine(ddnBabordX, ddnBabordY, (int)(ddnBabordX-longPelle*Math.cos(angleS)), (int)(ddnBabordY+longPelle*Math.sin(angleS)));
		drawAngleText(g, angleS);
		g.setColor(Color.blue);
		g.drawLine(ddnBabordX, ddnBabordY, (int)(ddnBabordX-longPelle*Math.cos(angleFM)), (int)(ddnBabordY+longPelle*Math.sin(angleFM)));
		drawAngleText(g, angleFM);
		
		//tribord
		g.setColor(Color.red);
		g.drawLine(ddnTribordX, ddnTribordY, (int)(ddnTribordX+longPelle*Math.cos(angleA-slipA)), (int)(ddnTribordY+longPelle*Math.sin(angleA-slipA)));
		g.drawLine(ddnTribordX, ddnTribordY, (int)(ddnTribordX+longPelle*Math.cos(angleS+slipS)), (int)(ddnTribordY+longPelle*Math.sin(angleS+slipS)));
		g.setColor(new Color(15,255,15));	//green
		g.drawLine(ddnTribordX, ddnTribordY, (int)(ddnTribordX+longPelle*Math.cos(angleA)), (int)(ddnTribordY+longPelle*Math.sin(angleA)));
		g.drawLine(ddnTribordX, ddnTribordY, (int)(ddnTribordX+longPelle*Math.cos(angleS)), (int)(ddnTribordY+longPelle*Math.sin(angleS)));
		g.setColor(Color.blue);
		g.drawLine(ddnTribordX, ddnTribordY, (int)(ddnTribordX+longPelle*Math.cos(angleFM)), (int)(ddnTribordY+longPelle*Math.sin(angleFM)));
	}
	
	private void drawAngleText(Graphics g, double angle) {
		g.drawChars((radToDeg(angle).toString()+"°").toCharArray(), 0, radToDeg(angle).toString().length()+1, (int)(ddnBabordX-2*boatWidth*Math.cos(angle)), (int)(ddnBabordY+2*boatWidth*Math.sin(angle)));
	}

	@Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        
        h=this.getHeight();
		w=this.getWidth();
		midX=w/2;
		midY=h/2;
		
		drawBoat(g);
		drawAmplitudes(g);
	}
	
	private Double radToDeg(double rad) {
		return arrondir(rad/Math.PI * 180);
	}

	
	private double arrondir(double val){ //arrondit a 10puissance -2
    	val = val*100;
    	val = Math.round(val);
        val = val/100;
        return val;
    }
}
