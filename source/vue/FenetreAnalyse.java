package vue;

import model.*;
import java.awt.Color;
import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

public class FenetreAnalyse extends JFrame{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2487199431961604507L;
	
	Donnees donnees;
	DonneesReference donneesRef;
	Athlete athlete;
	
	JPanel mainConteneur;
	JPanel conteneurTables;
	GrilleDonnees vueDonneesSortie;
	GrilleDonnees vueDonneesRameur;
	GrilleDonnees vueAttaque;
	GrilleDonnees vueDegage;
	GrilleDonnees vueAngle;
	VueGraphe graphDonnees;
	VueAmplitudes vueAmplitude;
	JPanel vueCourbeForce;
	

	public FenetreAnalyse(Donnees d, DonneesReference df, Athlete a) {
		super("Entrainement de "+a.getNom() +" "+a.getPrenom());
		this.setSize(950,950);
        this.setLocation(1570,75);
     //   this.setResizable(false);
        this.setVisible(true);
        
        mainConteneur = new JPanel(new GridLayout(1,2,0,0));
        mainConteneur.setBorder(new EmptyBorder(0,0,0,10));
        conteneurTables = new JPanel(new GridLayout(6,1,20,20));
        conteneurTables.setBackground(Color.gray);
        conteneurTables.setBorder(new EmptyBorder(10,5,20,5));

		donnees = d;
		donneesRef = df;
		athlete = a;

		JPanel infosGenerales = new JPanel(new GridLayout(4,1));
		JLabel dist = new JLabel(" Distance parcourue: "+donnees.distanceTravail+" m");
		JLabel tps = new JLabel(" Durée du travail: "+donnees.getDureeTravailString());
		JLabel intensite = new JLabel(" Intensité: "+donneesRef.getTypeEntrainement());
		JLabel vitAu500 = new JLabel(" Split Moyen: " + tempsAu500(donnees.vitesseMoy)+"/500m");
		infosGenerales.add(dist);
		infosGenerales.add(tps);
		infosGenerales.add(intensite);
		infosGenerales.add(vitAu500);
		conteneurTables.add(infosGenerales);
		
		try {
			vueDonneesSortie = new GrilleDonnees("Données Sortie", getNomsDonneesSortie(), getDonneesSortie(), getRefSortie());
			vueAttaque = new GrilleDonnees("Données Attaque", getNomsDonneesAttaque(),getDonneesAttaque(), getRefsAttaque());
			vueDegage = new GrilleDonnees("Données Dégagé", getNomsDonneesDegage(),getDonneesDegage(), getRefsDegage());
			vueAngle = new GrilleDonnees("Données Amplitude", getNomsDonneesAmplitude(),getDonneesAmplitude(), getRefsAmplitude());
			vueDonneesRameur = new GrilleDonnees("Données Rameur",getNomsDonneesRameur(), getDonneesRameur());
			graphDonnees = new VueGraphe(getNomsDonneesGraph(), getDonneesGraph(), getRefsGraph());
			conteneurTables.add(vueDonneesSortie);
			conteneurTables.add(vueAttaque);
			conteneurTables.add(vueDegage);
			conteneurTables.add(vueAngle);
			conteneurTables.add(vueDonneesRameur);
			mainConteneur.add(conteneurTables);
			mainConteneur.add(graphDonnees);
			if(donnees.boat.contains("1x")) {
				vueAmplitude = new VueAmplitudes(donnees.angleAttaqueMoy, donnees.angleForceMaxMoy, donnees.angleSortieMoy, donnees.slipAttaqueMoy, donnees.slipSortieMoy);
				mainConteneur.add(vueAmplitude);
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		this.setContentPane(mainConteneur);
	}
	
	private List<Double> getDonneesSortie(){
		List<Double> dS = new ArrayList<Double>();
		dS.add(donnees.vitesseMoy);
		dS.add(donnees.vitesseMoy*3.6);
		dS.add((double)donnees.nbCoups);
		dS.add(arrondir(donnees.distanceTravail/donnees.nbCoups));
		dS.add(arrondir(donnees.nbCoups/donnees.getDureeTravailMinutes()));
		return dS;
	}
	
	private List<Double> getRefSortie(){
		List<Double> dS = new ArrayList<Double>();
		dS.add(arrondir(donneesRef.getVitesseMS()*donneesRef.getIntensiteMax()));
		dS.add(arrondir(donneesRef.getVitesseKMH()*donneesRef.getIntensiteMax()));
		dS.add(arrondir(donnees.getDureeTravailMinutes()*donneesRef.getCadence()));
		
		if(donneesRef.getCadence() == 0.0) {
			dS.add(arrondir(0.0));
		}else {
			dS.add(arrondir(donnees.distanceTravail/(donnees.getDureeTravailMinutes()*donneesRef.getCadence())));
		}
		
		dS.add(arrondir(donneesRef.getCadence()));
		return dS;
	}
	
	private List<String> getNomsDonneesSortie() {
		List<String> nDS = new ArrayList<String>();
		nDS.add("Vitesse moyenne (m/s)");
		nDS.add("Vitesse moyenne (km/h)");
		nDS.add("Nombre de coups");
		nDS.add("distance par coups (m)");
		nDS.add("Cadence moyenne (cps/min)");
		return nDS;
	}
	
	private List<Double> getDonneesRameur(){
		List<Double> dR = new ArrayList<Double>();
		dR.add(donnees.puissanceMoy);
		dR.add(Collections.max(donnees.puissances));
		dR.add(donnees.forceMoy);
		dR.add(donnees.forceMaxMoy);
		dR.add(donnees.workMoy);
		dR.add(donnees.angleForceMaxMoy);
		return dR;
	}
	
	private List<String> getNomsDonneesRameur() {
		List<String> nDR = new ArrayList<String>();
		nDR.add("Puissance (Watts)");
		nDR.add("Puissance Max relevée (Watts)");
		nDR.add("Moyenne de Force le long d'un coup(Newton)");
		nDR.add("Pic de Force par coup (Newton)");
		nDR.add("Travail (Joule)");
		nDR.add("Angle de Force Max (°)");
		return nDR;
	}
	
	private List<Double> getDonneesAttaque(){
		List<Double> dA = new ArrayList<Double>();
		dA.add(donnees.angleAttaqueMoy);
		dA.add(donnees.slipAttaqueMoy);
		dA.add(donnees.angleAttaqueMoy + donnees.slipAttaqueMoy);
		return dA;
	}
	
	private List<Double> getRefsAttaque(){
		List<Double> dA = new ArrayList<Double>();
		dA.add(donnees.angleAttaqueMoy);
		dA.add(donneesRef.getSlipAvant());
		dA.add(arrondir(donnees.angleAttaqueMoy + donneesRef.getSlipAvant()));
		return dA;
	}
	
	private List<String> getNomsDonneesAttaque() {
		List<String> nDA = new ArrayList<String>();
		nDA.add("Angle d'attaque (°)");
		nDA.add("Dérapagage avant (°)");
		nDA.add("Amplitude efficace avant (°)");
		return nDA;
	}
	
	private List<Double> getDonneesDegage(){
		List<Double> dD = new ArrayList<Double>();
		dD.add(donnees.angleSortieMoy);
		dD.add(donnees.slipSortieMoy);
		dD.add(donnees.angleSortieMoy-donnees.slipSortieMoy);
		return dD;
	}
	
	private List<Double> getRefsDegage(){
		List<Double> dD = new ArrayList<Double>();
		dD.add(donnees.angleSortieMoy);
		dD.add(donneesRef.getSlipArriere());
		dD.add(arrondir(donnees.angleSortieMoy - donneesRef.getSlipArriere()));
		return dD;
	}
	
	private List<String> getNomsDonneesDegage() {
		List<String> nDD = new ArrayList<String>();
		nDD.add("Angle de sortie (°)");
		nDD.add("Dérapagage arrière (°)");
		nDD.add("Amplitude efficace arrière (°)");
		return nDD;
	}
	
	private List<Double> getDonneesAmplitude(){
		List<Double> dAmp = new ArrayList<Double>();
		double ampGestuelle = arrondir(donnees.angleSortieMoy + -donnees.angleAttaqueMoy);
		double ampEfficace = arrondir(donnees.angleSortieMoy - donnees.slipSortieMoy + -donnees.angleAttaqueMoy - donnees.slipAttaqueMoy);
		dAmp.add(ampGestuelle);
		dAmp.add(ampEfficace);
		dAmp.add(arrondir(ampEfficace/ampGestuelle * 100));
		return dAmp;
	}
	
	private List<Double> getRefsAmplitude(){
		List<Double> dAmp = new ArrayList<Double>();
		double ampGestuelle = arrondir(donnees.angleSortieMoy + -donnees.angleAttaqueMoy);
		double ampEfficace = arrondir(donnees.angleSortieMoy - donneesRef.getSlipArriere() + -donnees.angleAttaqueMoy - donneesRef.getSlipAvant());
		dAmp.add(ampGestuelle);
		dAmp.add(ampEfficace);
		dAmp.add(arrondir(ampEfficace/ampGestuelle * 100));
		return dAmp;
	}
	
	private List<String> getNomsDonneesAmplitude() {
		List<String> nDAmp = new ArrayList<String>();
		nDAmp.add("Amplitude gestuelle (°)");
		nDAmp.add("Amplitude efficace(°)");
		nDAmp.add("Efficacité balayage (%)");
		return nDAmp;
	}
	
	private List<String> getNomsDonneesGraph(){
		List<String> nDGraph = new ArrayList<String>();
		nDGraph.add("Amplitude efficace Avant");
		nDGraph.add("Amplitude efficace Arrière");
		nDGraph.add("Amplitude efficace");
		nDGraph.add("Vitesse moyenne");
		if(donneesRef.getCadence() != 0.0) {
			nDGraph.add("Distance par coups");
			nDGraph.add("Cadence moyenne");
		}
		
		return nDGraph;
	}
	
	private List<Double> getDonneesGraph(){
		List<Double> dGraph = new ArrayList<Double>();
		dGraph.add(donnees.angleAttaqueMoy + donnees.slipAttaqueMoy);
		dGraph.add(donnees.angleSortieMoy-donnees.slipSortieMoy);
		dGraph.add(arrondir(donnees.angleSortieMoy - donnees.slipSortieMoy + -donnees.angleAttaqueMoy - donnees.slipAttaqueMoy));
		dGraph.add(donnees.vitesseMoy);
		if(donneesRef.getCadence() != 0.0) {
			dGraph.add(arrondir(donnees.distanceTravail/donnees.nbCoups));
			dGraph.add(arrondir(donnees.nbCoups/donnees.getDureeTravailMinutes()));
		}
		return dGraph;
	}
	
	private List<Double> getRefsGraph(){
		List<Double> dRefGraph = new ArrayList<Double>();
		dRefGraph.add(arrondir(donnees.angleAttaqueMoy + donneesRef.getSlipAvant()));
		dRefGraph.add(arrondir(donnees.angleSortieMoy - donneesRef.getSlipArriere()));
		dRefGraph.add(arrondir(donnees.angleSortieMoy - donneesRef.getSlipArriere() + -donnees.angleAttaqueMoy - donneesRef.getSlipAvant()));
		dRefGraph.add(arrondir(donneesRef.getVitesseMS()*donneesRef.getIntensiteMax())); 
		if(donneesRef.getCadence() != 0.0) {
			dRefGraph.add(arrondir(donnees.distanceTravail/(donnees.getDureeTravailMinutes()*donneesRef.getCadence())));
			dRefGraph.add(arrondir(donneesRef.getCadence()));
		}
		return dRefGraph;
	}
	
	private double arrondir(double val){ //arrondit a 10puissance -2
    	val = val*100;
    	val = Math.round(val);
        val = val/100;
        return val;
    }
	
	 private String tempsAu500(double vitesseMparS){
			double nbSecondes = 500/vitesseMparS;
			int nbMinutes = (int)nbSecondes/60;
			nbSecondes = arrondir(nbSecondes-60*nbMinutes);
			return (nbMinutes+":"+nbSecondes);
		}
	
}
