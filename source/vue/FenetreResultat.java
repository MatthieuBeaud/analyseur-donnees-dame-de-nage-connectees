package vue;

import model.*;
import javax.swing.*;

public class FenetreResultat extends JFrame{
    
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JTextArea afficheID;
    private JTextArea affichexcel;
    
    private Athlete athlete;
    private Donnees donnees;
    
    public FenetreResultat(Athlete a, Donnees d){
        super("Entrainement de "+a.getNom() +" "+a.getPrenom());
        this.setSize(1000,600);
        this.setLocation(580,75);
        this.setLayout(null);
        this.setResizable(false);
        this.setVisible(true);
        
        athlete = a;
        donnees = d;
        
        afficheID = new JTextArea();
        afficheID.setBounds(10,10,250,250);
        afficheID.setEditable(false);
        
        affichexcel = new JTextArea();
        affichexcel.setBounds(300,10,650,550);
        affichexcel.setEditable(false);
        
        JPanel conteneur= new JPanel();
        conteneur.setLayout(null);
        conteneur.add(affichexcel);
        conteneur.add(afficheID);
        this.setContentPane(conteneur);
        
        setTexteAthlete();
        setTexteResultats();
    }
    
    public void setTexteResultats() {
    	affichexcel.setText(donnees.getResultText());
    }
    
    private void setTexteAthlete() {
    	afficheID.setText("Identite: \n \n");
        afficheID.append("Nom : "+ (athlete.getNom())+ "\n"+"Prenom : " + (athlete.getPrenom()) +"\n" +"Taille : "+ (athlete.getTaille())+" cm \n" +"Poids: " + (athlete.getPoids()) +" kg \nCategorie: "+athlete.categorie);
    }
    
    
    
}
