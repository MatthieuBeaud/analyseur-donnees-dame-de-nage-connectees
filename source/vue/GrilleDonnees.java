package vue;

import java.awt.Color;
import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

public class GrilleDonnees extends JPanel{
	/**
	 * 
	 */
	private static final long serialVersionUID = -1536524286728380352L;
	List<String> nomsLignes;
	List<Double> valeursMoyennes;
	List<Double> valeursObjectifs;
	List<Double> valeursAnalyses;
	int nbLignes;
	JLabel titreLabel;
	JLabel moyLabel;
	JLabel objLabel;
	JLabel analyseLabel;
	

	public GrilleDonnees(String titre, List<String> dataNames, List<Double> dataValues, List<Double> dataObjectifs) {
		super();
		nbLignes = dataNames.size()+1;
		nomsLignes = dataNames;
		valeursMoyennes = dataValues;
		valeursObjectifs = dataObjectifs;
		
		this.setLayout(new GridLayout(nbLignes, 4,5,5));
		this.setBackground(Color.gray);
		titreLabel = new JLabel(titre,SwingConstants.CENTER);
		titreLabel.setOpaque(true);
		titreLabel.setBackground(Color.lightGray);
		moyLabel = new JLabel("Moyenne",SwingConstants.CENTER);
		moyLabel.setOpaque(true);
		moyLabel.setBackground(Color.lightGray);
		objLabel = new JLabel("Objectif",SwingConstants.CENTER);
		objLabel.setOpaque(true);
		objLabel.setBackground(Color.lightGray);
		analyseLabel = new JLabel("Analyse",SwingConstants.CENTER);
		analyseLabel.setOpaque(true);
		analyseLabel.setBackground(Color.lightGray);
		this.add(titreLabel);
		this.add(moyLabel);
		this.add(objLabel);
		this.add(analyseLabel);
		
		setAnalyseValues();
		displayContent();
	}
	
	
	public GrilleDonnees(String titre, List<String> dataNames, List<Double> dataValues) {
		super();
		nbLignes = dataNames.size()+1;
		nomsLignes = dataNames;
		valeursMoyennes = dataValues;
		valeursObjectifs = new ArrayList<Double>();
		
		this.setLayout(new GridLayout(nbLignes, 2,5,5));
		this.setBackground(Color.gray);
		titreLabel = new JLabel(titre,SwingConstants.CENTER);
		titreLabel.setOpaque(true);
		titreLabel.setBackground(Color.lightGray);
		moyLabel = new JLabel("Moyenne",SwingConstants.CENTER);
		moyLabel.setOpaque(true);
		moyLabel.setBackground(Color.lightGray);

		this.add(titreLabel);
		this.add(moyLabel);

		
		displayWithoutRef();
	}
	
	private void displayWithoutRef() {
		for(int i=0; i<nbLignes-1; i++) {
			JLabel nom = new JLabel(nomsLignes.get(i),SwingConstants.CENTER);
			nom.setOpaque(true);
			JLabel moy = new JLabel(valeursMoyennes.get(i).toString(),SwingConstants.CENTER);
			moy.setOpaque(true);
			
			this.add(nom);
			this.add(moy);
		}
	}
	
	private void displayContent() {
		for(int i=0; i<nbLignes-1; i++) {
			JLabel nom = new JLabel(nomsLignes.get(i),SwingConstants.CENTER);
			nom.setOpaque(true);
			JLabel moy = new JLabel(valeursMoyennes.get(i).toString(),SwingConstants.CENTER);
			moy.setOpaque(true);
			
			Double valObj = valeursObjectifs.get(i);
			JLabel obj;
			if(valObj.equals(0.0)) {
				obj = new JLabel("Libre",SwingConstants.CENTER);
			}else {
				obj = new JLabel(valeursObjectifs.get(i).toString(),SwingConstants.CENTER);
			}
			
			obj.setOpaque(true);
			Double analyse = valeursAnalyses.get(i);
			JLabel ana = new JLabel(String.format("%.2f",analyse)+" %",SwingConstants.CENTER);
			ana.setOpaque(true);
			if(analyse.equals(Double.POSITIVE_INFINITY)) {
				ana.setBackground(Color.LIGHT_GRAY);
			}else if(analyse>95 && analyse<105) {
				ana.setBackground(Color.GREEN);
			}else if(analyse>85 && analyse<115) {
				ana.setBackground(Color.ORANGE);
			}else {
				ana.setBackground(Color.RED);
			}
			this.add(nom);
			this.add(moy);
			this.add(obj);
			this.add(ana);
		}
	}

	private void setAnalyseValues() {
		valeursAnalyses = new ArrayList<Double>();
		for(int i=0; i<nbLignes-1; i++) {
			valeursAnalyses.add(valeursMoyennes.get(i)/valeursObjectifs.get(i)*100);
		}
	}
	
}
