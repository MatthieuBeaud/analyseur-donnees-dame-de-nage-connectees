package model;
//import org.apache.poi.hssf.usermodel.HSSFWorkbook;
//import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import helper.ExcelReader;

import java.io.File;
import java.io.IOException;
import java.sql.Time;
import java.util.ArrayList;
import java.util.List;

public class Donnees {
    
    private Athlete athlete;
    private XSSFWorkbook workb;
    private XSSFSheet sheet;
    
    
    private String date;
    public double[] paramPelle;
    public String boat;
    
    private List<Double> vitesses; //en m/s
    public double vitesseMoy;
    public double distanceTravail;
    public int dureeHeures;
    public int dureeMinutes;
    public double dureeSecondes;
    public int nbCoups;
    
    public List<Double> puissances; //en Watt
    public double puissanceMoy;
    private List<Double> forces;    //en Newton
    public double forceMoy;
    private List<Double> forcesMax;     //en Newton
    public double forceMaxMoy;
    private List<Double> works;		// en Joule
    public double workMoy;
    private List<Double> anglesForceMax; //en degres
    public double angleForceMaxMoy; 
    
    private List<Double> anglesAttaque;
    public double angleAttaqueMoy;
    private List<Double> anglesSortie;
    public double angleSortieMoy;
    private List<Double> slipsAttaque;
    public double slipAttaqueMoy;
    private List<Double> slipsSortie;
    public double slipSortieMoy;
    
   
    public List<String> textes;
    
    
    public Donnees (String nomF, Athlete athlete, String boat) throws IOException, Exception{
        this.athlete = athlete;
        this.boat = boat;
        workb = ExcelReader.openExcel(new File(nomF));
        sheet = ExcelReader.getSheet(workb, 0);
        
        initData();
    }
    
    public String getDureeTravailString() {
    	return dureeHeures+"h "+dureeMinutes+"min "+arrondir(dureeSecondes)+"s";
    }
    
    public Time getDureeTravail() {
    	return new Time((long) (dureeHeures*3600*1000+dureeMinutes*60*1000+dureeSecondes*1000));
    }
    
    public Double getDureeTravailMinutes() {
    	return arrondir(dureeHeures*60+dureeMinutes+dureeSecondes/60);
    }
    
    public String getResultText() {
        String text="";
        for(String ligne : textes){
			text += ligne;
		}
        return text;
    }
    
    private void initData(){
        try{
            initDateSortie();
            initParamPelle();
            
            int lastValidLine = findLastValidLine();
            distanceTravail = Double.parseDouble(ExcelReader.getCellContent(sheet, 1, lastValidLine));
            String dureeTravail= ExcelReader.getCellContent(sheet, 3, lastValidLine);
            dureeHeures= Integer.parseInt(dureeTravail.substring(0,2));
            dureeMinutes= Integer.parseInt(dureeTravail.substring(3,5));
            dureeSecondes= Float.parseFloat(dureeTravail.substring(6,10));
            
            nbCoups= (int)Math.round(ExcelReader.getCellValue(sheet, 9, lastValidLine));
           
            initVitesses();          
            initPuissances();            
            initForces();            
            initForcesMax();            
            initWorks();
            initAnglesAttaque();
            initSlipsAttaque();
            initAnglesSortie();
            initSlipsSortie();
            initAnglesForceMax(); 
            
            initText();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    
    private int findLastValidLine() {
    	int lastValidLine = sheet.getLastRowNum();
    	String distanceStr;
		String dureeTravail;
		Double nbCoupsStr;
    	try {
    		distanceStr = ExcelReader.getCellContent(sheet, 1, lastValidLine);
    		dureeTravail= ExcelReader.getCellContent(sheet, 3, lastValidLine);
    		nbCoupsStr= ExcelReader.getCellValue(sheet, 9, lastValidLine);
    	}catch(Exception ex) {
    		distanceStr = "---";
        	dureeTravail= "---";
        	nbCoupsStr = 0.0; 
    	}
    	while((distanceStr.equals("---") || dureeTravail.equals("---") || nbCoupsStr.equals(0.0)) && lastValidLine>=0) {
    		lastValidLine--;
    		try {
	    		distanceStr = ExcelReader.getCellContent(sheet, 1, lastValidLine);
	        	dureeTravail= ExcelReader.getCellContent(sheet, 3, lastValidLine);
	        	nbCoupsStr= ExcelReader.getCellValue(sheet, 9, lastValidLine);
    		}catch (Exception e){
    			System.err.println("la ligne "+ lastValidLine +" n'est pas une derniere ligne valide");
    			System.err.println(e.toString());
    		}
    	}
    	if(lastValidLine==-1) {
    		System.err.println("Aucune ligne valide dans ce fichier");
    	}
    	return lastValidLine;
    }
    
    private void initDateSortie() throws Exception{
    	date = ExcelReader.getCellContent(sheet, 1, 3);
    }
    
    private void initParamPelle(){
    	 paramPelle= new double[2];
    	 try {
        	paramPelle[0]= ExcelReader.getCellValue(sheet, 13, 5);
        	paramPelle[1]= ExcelReader.getCellValue(sheet, 13, 6);
    	 }catch(Exception e) {
    		 e.printStackTrace();
    	 }
    }
    
    private void initVitesses() throws Exception{
    	vitesses = ExcelReader.getColValuesFromStrings(sheet, 5, 30);
    	vitesseMoy = arrondir(moyenne(vitesses));
    }
    
    private void initPuissances() throws Exception{
    	puissances = ExcelReader.getColValues(sheet, 13, 30);
    	puissanceMoy = arrondir(moyenne(puissances));
    }
    
    private void initForces() throws Exception{
    	forces = ExcelReader.getColValues(sheet, 18, 30);
    	forceMoy = arrondir(moyenne(forces));
    }
    
    private void initForcesMax() throws Exception{
    	forcesMax = ExcelReader.getColValues(sheet, 20, 30);
    	forceMaxMoy = arrondir(moyenne(forcesMax));
    }
    
    private void initWorks() throws Exception{
    	works = ExcelReader.getColValues(sheet, 19, 30);
    	workMoy = arrondir(moyenne(works));
    }
    
    private void initAnglesAttaque() throws Exception{
    	anglesAttaque = ExcelReader.getColValues(sheet, 14, 30);
    	angleAttaqueMoy = arrondir(moyenne(anglesAttaque));
    }
    
    private void initSlipsAttaque() throws Exception{
    	slipsAttaque = ExcelReader.getColValues(sheet, 15, 30);
    	slipAttaqueMoy = arrondir(moyenne(slipsAttaque));
    }
    
    private void initAnglesSortie() throws Exception{
    	anglesSortie = ExcelReader.getColValues(sheet, 16, 30);
    	angleSortieMoy = arrondir(moyenne(anglesSortie));
    }
    
    private void initSlipsSortie() throws Exception{
    	slipsSortie = ExcelReader.getColValues(sheet, 17, 30);
    	slipSortieMoy = arrondir(moyenne(slipsSortie));
    }
    
    private void initAnglesForceMax() throws Exception{
    	anglesForceMax = ExcelReader.getColValues(sheet, 21, 30);
    	angleForceMaxMoy = arrondir(moyenne(anglesForceMax));
    }
    
    private void initText() {
    	textes = new ArrayList<String>();
        textes.add("Date: "+date+" \n \nLongueur totale pelle: "+paramPelle[0]+ " cm \nLevier: "+paramPelle[1]+" cm \n \n");
        textes.add("Distance du travail: "+distanceTravail+" m \n");
        textes.add("Duree du travail: "+dureeHeures+" h "+dureeMinutes+" min "+arrondir(dureeSecondes)+" s \n \n");
        textes.add("Vitesse moyenne: "+vitesseMoy+" m/s \n");
        textes.add("Temps moyen au 500m: "+tempsAu500()+" /500m \n");
        textes.add("Nombre de coups: "+nbCoups+" \n");
        textes.add("Cadence moyenne: "+cadmoyenne()+" coups par minute \n \n");
        textes.add("Puissance moyenne: "+puissanceMoy+" Watt \n");
        textes.add("Moyenne de la force moyenne par coup: "+forceMoy+" Newton \n");
        textes.add("Moyenne de la force max (pic) par coup: "+forceMaxMoy+" Newton \n");
        textes.add("Travail moyen par coup: "+workMoy+" Joules \n \n");
        textes.add("Moyenne de l'angle d'application de force max: "+angleForceMaxMoy+"° \n");
        textes.add("Moyenne de l'angle d'attaque: "+angleAttaqueMoy+"° \n");
        textes.add("Moyenne de derapage a l'attaque: "+slipAttaqueMoy+"° \n");
        textes.add("Moyenne de l'angle de sortie: "+angleSortieMoy+"° \n");
        textes.add ("Moyenne de derapage a la sortie: "+slipSortieMoy+"° \n");
        textes.add("Moyenne l'amplitude gestuelle: "+amplitude()+"° \n");
        textes.add("Moyenne de l'amplitude efficace: "+amplitudeEfficace()+"° \n");
        textes.add("Perte d'amplitude: "+angleInefficace()+" ° \n");
        textes.add("Efficacite du balayage: "+efficacite()+" % \n \n");
        textes.add("(les derapages avant et arriere sont calcules a partir des angles ou la force appliquee est inferieure a 200 Newton) \n \n");
        textes.add("Rapport poids/puissance: "+poidsPuissance()+" Watt/kg \n");
        textes.add("Rapport poids/vitesse: "+poidsVitesse()+" m/s/kg \n");
    }
    
    
    private double arrondir(double val){ //arrondit a 10puissance -2
    	val = val*100;
    	val = Math.round(val);
        val = val/100;
        return val;
    }
    
    private double moyenne(List<Double> values) {
    	double somme = 0;
    	for(double value: values) {
    		somme += value;
    	}
    	return somme/values.size();
    }
    
    private String tempsAu500(){
		double nbSecondes = 500/vitesseMoy;
		int nbMinutes = (int)nbSecondes/60;
		nbSecondes = arrondir(nbSecondes-60*nbMinutes);
		return (nbMinutes+":"+nbSecondes);
	}
	
    private double amplitude(){
		return arrondir(Math.abs(angleAttaqueMoy-angleSortieMoy));
	}
	
    private double amplitudeEfficace(){
		return arrondir(amplitude()-slipAttaqueMoy-slipSortieMoy);
	}
	
    private double angleInefficace(){
		return arrondir(amplitude()-amplitudeEfficace());
	}
	
    private double efficacite(){
		return arrondir((1-angleInefficace()/amplitude())*100);
	}
	
    private double poidsPuissance(){
		return arrondir(puissanceMoy/athlete.getPoids());
	}
    
    private double poidsVitesse(){
		return arrondir(vitesseMoy/athlete.getPoids());
	}
	
	public double cadmoyenne(){
		return arrondir(nbCoups/(dureeHeures*60+dureeMinutes+dureeSecondes/60));
	}
	
	public String laDate(){
		date=date.substring(0,2)+"-"+date.substring(3,5)+"-"+date.substring(6,8);
		return date;
	}
	
	
}
