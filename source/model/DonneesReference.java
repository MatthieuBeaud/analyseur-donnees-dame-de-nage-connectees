package model;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URL;
import java.net.URLClassLoader;
import java.sql.Time;
import java.sql.Timestamp;
import java.time.Duration;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import helper.ExcelReader;

@SuppressWarnings("unused")
public class DonneesReference {

    private XSSFWorkbook workb;
	private XSSFSheet sheet;
    
	private Athlete athlete;
	private String entrainement;

    private double cadence;
	private double intensiteMin;
	private double intensiteMax;
    //private double distParCoup;
    private double vitesseMS;
    private double vitesseKMH;
    
    private double slipAvant;
    private double slipArriere;
    
    public DonneesReference(String nomFichierRef, Athlete athlete, String entrainement) throws IOException, Exception{
        this.athlete = athlete;
        this.entrainement = entrainement;
        try {
        	workb = ExcelReader.openExcel(new File(nomFichierRef));
        	
        	sheet = ExcelReader.getSheet(workb, 0); 

            initData();
            
        }catch (Exception e) {
        	e.printStackTrace();
        }
    }
    
    public double getVitesseMS() {
    	return vitesseMS;
    }
    
    public double getVitesseKMH() {
    	return vitesseKMH;
    }
    
    public double getSlipAvant() {
    	return slipAvant;
    }
    
    public double getSlipArriere() {
    	return slipArriere;
    }
    
    public double getIntensiteMin() {
    	return intensiteMin;
    }
    
    public double getIntensiteMax() {
    	return intensiteMax;
    }
    
    public double getCadence() {
    	return cadence;
    }
    
    public String getTemps500m() {
    	return tempsAu500();
    }
    
    public String getTypeEntrainement() {
    	return entrainement;
    }
    
    private void initData() throws Exception{
       int numLigne = ExcelReader.findLine(sheet, 0, athlete.categorie, 1, athlete.boatClass);
       switch(entrainement) {
       case "B1":
    	   cadence = 18;
    	   intensiteMin = 0.7;
    	   intensiteMax = 0.75;
    	   break;
       case "B2":
    	   cadence = 20;
    	   intensiteMin = 0.76;
    	   intensiteMax = 0.8;
    	   break;
       case "B3":
    	   cadence = 24;
    	   intensiteMin = 0.81;
    	   intensiteMax = 0.85;
    	   break;
       case "B4":
    	   cadence = 0;
    	   intensiteMin = 0.86;
    	   intensiteMax = 0.95;
    	   break;
       case "B5":
    	   cadence = 0;
    	   intensiteMin = 1.0;
    	   intensiteMax = 1.1;
    	   break;
       case "Course":
    	   cadence = 0;
    	   intensiteMin = 1;
    	   intensiteMax = 1;
    	   break;
       }
       
       Timestamp temps2000m = new Timestamp(ExcelReader.getCellTime(sheet, 2, numLigne).getTime());
       vitesseMS = findVitesseMSFromTps2000m(temps2000m);
       vitesseKMH = vitesseMS*3.6;
       
       slipAvant = ExcelReader.getCellValue(sheet, 3, numLigne);
       slipArriere = ExcelReader.getCellValue(sheet, 4, numLigne);
    }
    
    @SuppressWarnings("deprecation")
	private double findVitesseMSFromTps2000m(Timestamp t) {
    	double totalS = t.getMinutes()*60+t.getSeconds();
    	//s/2000m
    	double allure = totalS/2;
    	//s/km
    	double vitesse = 1/allure;
    	//km/s
    	vitesse = vitesse*1000;
    	//m/s
    	return vitesse;	
    }
    
    private double arrondir(double val){ //arrondit a 10puissance -2
    	val = val*100;
    	val = Math.round(val);
        val = val/100;
        return val;
    }
    
    private String tempsAu500(){
		double nbSecondes = 500/vitesseMS;
		int nbMinutes = (int)nbSecondes/60;
		nbSecondes = arrondir(nbSecondes-60*nbMinutes);
		return (nbMinutes+":"+nbSecondes);
	}
}
