package model;
public class Athlete{
    private int taille;
    private float poids;
    private String nom;
    private String prenom;
    public String categorie;
    public String boatClass;
    
    public Athlete(int t, float p, String n, String pn, String cat, String boat){
        poids=p;
        taille=t;
        nom=n;
        prenom=pn;
        categorie = cat;
        boatClass = boat;
    }
    
    public String getNom(){
        return nom;
    }
    
    public String getPrenom(){ 
        return prenom;
    }
    
    public int getTaille(){
        return taille;
    }
    
    public float getPoids(){
        return poids;
    }    
}
