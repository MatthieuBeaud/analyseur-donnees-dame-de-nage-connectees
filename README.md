* [English](#user-guide)
* [Français](#guide-dutilisation)

# User Guide

## 1. Convert raw data

Here are the steps to follow to go from the raw data file (.csv) to a analysable excel file:
* Open the .csv file with excel

* Select the data cells, and go to Data, Convert

![Convert raw data](/img/convertRawToExcel1.jpg?raw=true)

* Choose the splitting type "", then "next"

![separation type](/img/convertRawToExcel2.jpg?raw=true)

* Choose the comma as splitting character, then "next"
* choisir la virgule comme charactère de séparation, puis "suivant"

![splitting character](/img/convertRawToExcel3.jpg?raw=true)

* Finish 

![Terminer](/img/convertRawToExcel4.jpg?raw=true)

* Now the data should be set in separate lines and columns
* Save the new file with the .xsls extension

## 2. Start the programm

Launch the AnalyseDameDeNage[Version].jar programm

## 3. Enter the training informations

![Enter the informations](/img/start.jpg?raw=true)

* Name 
* First Name
* Height (in cm)
* Weight (in kg)
* Choose the category
* Choose the boat
* Choose the type of training (B1: rate 18 with low intensity (UT2), B2: rate 20 with medium intensity (UT1), B3: rate 24 with high intensity (AT), B4: rate pyramid 24-28-32 over 2000m, B5: lactate speed work, Course: Racing)
* Choose the data file to analyse for this trainig: Open the file created and saved at the **[Step 1](#1-convert-raw-data)**
* Choose the reference data file: open the file "DonneeReference.xlsx"

**click on "Nouvel Entrainement"**

## 4. The Results

### Text Results

![Text Results](/img/textResult.jpg?raw=true)

### Detailled Results 

![Detailled Results](/img/visualResult.jpg?raw=true)

### Save the results

![Save Results](/img/saveTextResultAsFile.jpg?raw=true)

Once the results are displayed, a Button "Enregistrer" (Save) will appear on the first window (the one with the form).
It will save the text results in a seprate file.

<br/>
<br/>
<br/>

# Guide d'utilisation

## 1. Convertir les données

Convertir le fichier de données brutes (.csv) en fichier analysable par le programme:

* Ouvrir le fichier csv avec excel

* sélectionner les données, puis aller dans l'onglet Données, puis Convertir

![Convertir les données](/img/convertRawToExcel1.jpg?raw=true)

* choisir le type de séparation "Délimité", puis "suivant"

![type de séparation](/img/convertRawToExcel2.jpg?raw=true)

* choisir la virgule comme charactère de séparation, puis "suivant"

![charactère de séparation](/img/convertRawToExcel3.jpg?raw=true)

* Terminer 

![Terminer](/img/convertRawToExcel4.jpg?raw=true)

* Normalement les données sont maintenant présentées en lignes en colonnes séparées
* Enregistrer le fichier obtenu au format .xlsx 

## 2. Lancer le programme

Lancer le programme AnalyseDameDeNage[Version].jar

## 3. Entrer les informations de l'entrainement

![Entrer les informations](/img/start.jpg?raw=true)

* Nom 
* Prénom
* Taille (en cm)
* Poids (en kg)
* Choisir la catégorie
* Choisir l'embarcation
* Choisir le type d'entrainement
* Choisir le fichier de données de l'entrainement à analyser: Ouvrir le fichier créé et sauvegardé issu de **[l'étape 1](#1-convertir-les-données)**
* Choisir le fichier de données références en choisissant le fichier "DonneeReferences.xlsx" 

**cliquer sur Nouvel Entrainement**

## 4. Obtenir les résultats 

### Résultats textuels

![Résultats textuels](/img/textResult.jpg?raw=true)

### Résultats détaillés 

![Résultats détaillés](/img/visualResult.jpg?raw=true)

### Enregister les résultats 

![Save Results](/img/saveTextResultAsFile.jpg?raw=true)

Une fois les résultats affichés, un bouton "Enregistrer" sera disponible sur la fenêtre où l'on a entré les informations. 
Cette fonctionnalité permet d'enregistrer les resultats textuels dans un fichier.






